﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DamageTypes
{
    initialOnly,
    initialWithRepeatedSameDamageSameTick,
    initialWithRepeatedManyDamageSameTick,
    initialWithRepeatedSameDamageManyTick,
    initialWithRepeatedManyDamageManyTick,
    repeatedOnlySameDamageSameTick,
    repeatedOnlyManyDamageSameTick,
    repeatedOnlySameDamageManyTick,
    repeatedOnlyManyDamageManyTick
}

[System.Serializable]
public class DamageInfo
{
    [Tooltip("Every repeated damage tick will deal this amount of damage")]
    public float repeatedDamage;

    [Tooltip("Delay between when each Bleed Damage will deal damage in seconds")]
    public float timeBetweenTicks;

    [Tooltip("Number of times repeated damage is dealt")]
    public float damageRepeatNumber;

    [Tooltip("The delay between when the player shoots and when the damage is dealt (in seconds). Put to 0 for no delay")]
    public float initialDelay;

    [Tooltip("An array of how much Bleed Damage each tick will deal")]
    public float[] repeatedDamageArray;

    [Tooltip("Delay between when each Bleed Damage will deal damage in seconds")]
    public float[] timeBetweenTicksArray;

    [Tooltip("Amount of damage to be dealt initially. (Seperate from bleed damage)")]
    public int initialDamage;

    
    public DamageTypes WayDamageIsDealt;

    public void DoDamage(TakeDamage _target)
    {
        switch (WayDamageIsDealt)
        {
            case DamageTypes.initialOnly:
                _target.DoSingleDamage(initialDamage);
                break;
            case DamageTypes.initialWithRepeatedSameDamageSameTick:
                _target.DoSingleDamage(initialDamage, initialDelay);
                _target.DoTimedDamage(repeatedDamage, timeBetweenTicks, damageRepeatNumber);
                break;
            case DamageTypes.initialWithRepeatedManyDamageSameTick:
                _target.DoSingleDamage(initialDamage, initialDelay);
                _target.DoTimedDamage(repeatedDamageArray, timeBetweenTicks);
                break;
            case DamageTypes.initialWithRepeatedSameDamageManyTick:
                _target.DoSingleDamage(initialDamage, initialDelay);
                _target.DoTimedDamage(repeatedDamage, timeBetweenTicksArray);
                break;
            case DamageTypes.initialWithRepeatedManyDamageManyTick:
                _target.DoSingleDamage(initialDamage, initialDelay);
                _target.DoTimedDamage(repeatedDamageArray, timeBetweenTicksArray);
                break;
            case DamageTypes.repeatedOnlySameDamageSameTick:
                _target.DoTimedDamage(repeatedDamage, timeBetweenTicks,damageRepeatNumber);
                break;
            case DamageTypes.repeatedOnlyManyDamageSameTick:
                _target.DoTimedDamage(repeatedDamageArray, timeBetweenTicks);
                break;
            case DamageTypes.repeatedOnlySameDamageManyTick:
                _target.DoTimedDamage(repeatedDamage, timeBetweenTicksArray);
                break;
            case DamageTypes.repeatedOnlyManyDamageManyTick:
                _target.DoTimedDamage(repeatedDamageArray, timeBetweenTicksArray);
                break;
        }
    }
}

